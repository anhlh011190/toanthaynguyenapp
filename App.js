/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, {Component}   from 'react';
import { Provider }             from 'react-redux';
import { 
    createStore, applyMiddleware 
}                               from 'redux';
import thunk                    from 'redux-thunk';

import reducer                  from './react/reducers';
import { 
    Main
}                               from './react';
import { getCourses }           from './react';

const store = createStore(reducer, applyMiddleware(thunk));

store.dispatch(getCourses());

type Props = {};
class App extends Component<Props> {
    render() {
        return (
            <Provider store={store}>
                <Main />
            </Provider>
        );
    }
}

export default App;
