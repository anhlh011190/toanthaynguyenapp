import { AsyncStorage }         from 'react-native';
import DeviceInfo               from 'react-native-device-info';
import io                       from 'socket.io-client/dist/socket.io.js';
import config                   from "./../config";

let socket = io(config.apiUrl.socket ,{jsonp:false});
import {
    SET_COURSES, SET_LESSON,
    SET_COURSE, SET_USERINFO,
    CHANGE_ROUTE
}                               from '../ActionTypes';
import { 
    initCourses, initCourse,
    initLesson, login, buyCourseApi,
    getAsyncStorage, register
}                               from './../apis/app';

export function initApp(){
    return function( dispatch ) {
        getAsyncStorage("userinfo")
        .then(userinfo=>{
            if(userinfo != undefined){
                let u = JSON.parse(userinfo);
                let brand = DeviceInfo.getBrand();
                let systemVersion = DeviceInfo.getSystemVersion();
                let userAgent = DeviceInfo.getUserAgent();
                let uniqueId = DeviceInfo.getUniqueID();
                let deviceName = DeviceInfo.getDeviceName();
                login(u.phone, u.password, JSON.stringify({username: u.phone, password: u.password, brand, userAgent, systemVersion, uniqueId, deviceName})).then((rs)=>{
                    if(rs.success){
                        socket.emit('USER_JOIN',{id:"TTN"+rs.result.id});
                        rs.result.password = u.password;
                        AsyncStorage.setItem('userinfo',JSON.stringify(rs.result));
                        dispatch(setUserinfo(rs.result));
                    }else{
                        AsyncStorage.removeItem("userinfo");
                    }
                });
            }
        })
    }
}

export function submitRegister(data){
    return function( dispatch ) {
        register(data).then((rs)=>{
            if(rs.success){
                rs.result.password = data.password;
                AsyncStorage.setItem('userinfo',JSON.stringify(rs.result));
                dispatch(setUserinfo(rs.result));
                alert("Đăng ký tài khoản thành công!");
            }else{
                alert(rs.message);
            }
        });
    }
}

export function submitLogin(username, password){
    return function( dispatch ) {
        let brand = DeviceInfo.getBrand();
        let systemVersion = DeviceInfo.getSystemVersion();
        let userAgent = DeviceInfo.getUserAgent();
        let uniqueId = DeviceInfo.getUniqueID();
        let deviceName = DeviceInfo.getDeviceName();
        login(username, password,JSON.stringify({username, password, brand, userAgent, systemVersion, uniqueId, deviceName})).then((rs)=>{
            if(rs.success){
                rs.result.password = password;
                AsyncStorage.setItem('userinfo',JSON.stringify(rs.result));
                dispatch(setUserinfo(rs.result));
                alert("Đăng nhập thành công!");
            }else{
                alert(rs.message);
            }
        });
    }
}

export function submitLogout(){
    return function( dispatch ) {
        AsyncStorage.removeItem("userinfo");
        dispatch(setUserinfo({}));
    }
}

export function getCourses(){
    return function( dispatch ) {
        initCourses().then((rs)=>{
            if(rs.success){
                dispatch(setCourses(rs.result.lop12, 'lop12'));
                dispatch(setCourses(rs.result.lop11, 'lop11'));
                dispatch(setCourses(rs.result.lop10, 'lop10'));
            }
        });
    }
}

export function getCourse(idCourse){
    return function( dispatch ) {
        dispatch(setCourse({}));
        initCourse(idCourse).then((rs)=>{
            if(rs.success){
                dispatch(setCourse(rs.result));
            }else{
                dispatch(changeRoute("HOMEPAGE"));
                alert(rs.message);
            }
        });
        
    }
}

export function buyCourse(courseId){
    return function( dispatch, getState ) {
        let state = getState();
        if(state.app.userinfo.hasOwnProperty('id')){
            buyCourseApi(courseId,state.app.userinfo.id,state.app.userinfo.token).then((rs)=>{
                if(rs.success){
                    dispatch(setUserinfo(rs.result));
                    alert("Bạn đã mua khoá học thành công!");
                }else{
                    alert(rs.message);
                }
            });
        }else{
            alert("Bạn chưa đăng nhập!");
        }
    }
}

export function getLesson(idLesson){
    return function( dispatch, getState ) {
        let state = getState();
        if(state.app.userinfo.hasOwnProperty('id')){
            dispatch(setLesson({}));
            initLesson(idLesson,state.app.userinfo.id,state.app.userinfo.token).then((rs)=>{
                if(rs.success){
                    dispatch(setLesson(rs.result));
                }else{
                    dispatch(changeRoute("COURSE"));
                    alert(rs.message);
                }
            });
        }else{
            alert("Bạn chưa đăng nhập!");
        }
    }
}

export function changeRoute(route){
    return {
        type        : CHANGE_ROUTE,
        route
    };
}

function setCourses(courses,level){
    return {
        type        : SET_COURSES,
        courses,
        level
    };
}

function setUserinfo(userinfo){
    return {
        type        : SET_USERINFO,
        userinfo
    };
}

function setCourse(course){
    return {
        type        : SET_COURSE,
        course
    };
}

function setLesson(lesson){
    return {
        type        : SET_LESSON,
        lesson
    };
}