import { get, post }            from "./base";
import { AsyncStorage }         from 'react-native';
import config                   from "./../config";

export function initCourses(){
    return get(config.apiUrl.api + "/api/v2/course/home");
}

export function login(username, password, data){
    return post(config.apiUrl.api + "/api/v2/user/login", {username, password, data});
}

export function register(data){
    return post(config.apiUrl.api + "/api/v2/user/register", data);
}

export function buyCourseApi(courseId, userId, token){
    return post(config.apiUrl.api + "/api/v2/course/buy", {courseId, userId},token);
}

export function initCourse(idCourse){
    return get(config.apiUrl.api + "/api/v2/course/"+idCourse);
}

export function initLesson(idLesson, idUser = 1, token){
    return get(config.apiUrl.api + "/api/v2/lesson/"+idLesson+"/"+idUser, token);
}

export function getAsyncStorage(key){
    return new Promise((resolve, reject)=>{
        AsyncStorage.getItem(key,data=>JSON.parse(data))
        .then(d=>resolve(d))
        .catch(err=>{});
    });
}