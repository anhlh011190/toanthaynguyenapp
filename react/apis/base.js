import config                   from "./../config";

export function get(url, token = ""){
    return new Promise(function(resolve,reject){
        fetch( url , {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'SECRET': config.secret,
                "TOKEN": token
            },
            mode: 'cors'
        }).then(function(response) {
            if(response.status == 200){
                return response.json();
            }else{
                resolve({});
            }
        }).then(function(response){
            resolve(response);
        }).catch(err=>{
            reject({})
        });
    });
}

export function post(url,param,token=""){
    return new Promise(function(resolve,reject){
        fetch(url, {
            method: "POST",
            headers: {
                'user-agent': 'Mozilla/4.0 MDN Example',
                'content-type': 'application/json',
                'SECRET': config.secret,
                "TOKEN": token
            },
            body: JSON.stringify( param )
        }).then(function(response) {
            if(response.status == 200){
                return response.json();
            }else{
                return {};
            }
        }).then(function(response){
            resolve(response);
        }).catch(err=>{
            reject({})
        });
    });
}