import {StyleSheet} from 'react-native';

let styles = StyleSheet.create({
    navi:{
        backgroundColor:"#fafafa",
        flex:1
    },
    naviHeader:{
        alignSelf:"center",
        padding: 10
    },
    naviHeaderInfo:{
        justifyContent:"center",
        alignItems:"center"
    },
    naviHeaderImg:{
        width: 70, 
        height: 70,
        marginBottom:10
    },
    naviItem:{
        flexDirection:"row",
        backgroundColor:"#fff",
        padding: 5,
        marginBottom: 1
    },
    naviItemIcon:{
        color:"#3b5998",
        marginRight:5,
        marginLeft: 10,
    },
    naviItemText:{
        alignSelf:"center"
    },

    lessonTextWrap:{
        flexDirection:"row",
        margin:5
    },
    lessonText:{
        backgroundColor:"#3B5997",
        padding:5,
        color:"#fff",
        marginRight: 10
    },

    header:{
        flexDirection:"row",
        justifyContent: 'flex-end',
        backgroundColor:"#3b5998",
        padding:5
    },
    headerText:{
        color: '#fff',
        alignSelf:"center", 
        width:"85%", 
        textAlign:"center"
    },
    headerIcon:{
        color: '#fff',
        alignSelf:"flex-start"
    },
    headerIconBack:{
        color: '#fff',
        alignSelf:"center"
    },

    homepage:{
        padding:2,
        backgroundColor:"#fff"
    },
    homepageCourseTitle:{
        fontWeight:"bold",
        marginBottom:5,
        marginTop:5
    },
    homepageCourseList:{
        flexDirection:"row",
        marginBottom:10
    },
    homepageCourseListImgs:{
        flex:3
    },
    homepageCourseListImg:{
        width: "100%", 
        height: 70
    },
    homepageCourseListContent:{
        flex:6,
        padding:2
    },
    homepageCourseListContentText:{
        fontSize:12
    },

    loginWrap:{
        backgroundColor:"#2b3d4b"
    },
    login:{
        justifyContent:"center",
        alignItems:"center"
    },
    loginInput: {
        flexDirection: 'row',
        width: 250,
        height: 40,
        backgroundColor: 'rgba(1,1,1,0.3)',
        borderRadius: 20,
        marginTop: 20,
        marginBottom: 5,
        alignItems:'center'
    },   
    loginIcon: {
        flex: 1.2,
        marginLeft: 12,
        color: '#fff',
        fontSize: 20
    },
    loginTextInput: {
        color: 'white',        
        fontSize: 16,
        flex: 8.8
    },
    loginText:{
        marginTop:20,
        fontSize:24,
        color:"#fff"
    },
    loginButton: {
        backgroundColor: '#4caf50',
        borderRadius: 20,
        height: 40,
        width: 250,
        justifyContent: 'center',
        alignItems: 'center',
        overflow: 'hidden',
        elevation: 5
    },

    textBold:{
        fontWeight:"bold"
    },
    textPadding2:{
        paddingLeft:10
    },
    textActive:{
        color:"red",
        fontWeight: 'bold'
    },
    textCenter:{
        alignContent:"center"
    },

    btnPrimary:{
        backgroundColor: "#4263a3",
        borderRadius:5,
        width:100,
        padding:5,
        textAlign:"center",
        color:"#fff"
    },
    btnSuccess:{
        backgroundColor: "#5cb85c",
        borderRadius:5,
        width:100,
        padding:5,
        textAlign:"center",
        color:"#fff"
    }
});

export default styles;