export const SET_COURSES    = 'SET_COURSES';
export const SET_COURSE     = 'SET_COURSE';
export const CHANGE_ROUTE   = 'CHANGE_ROUTE';
export const SET_LESSON     = 'SET_LESSON';
export const SET_USERINFO   = 'SET_USERINFO';