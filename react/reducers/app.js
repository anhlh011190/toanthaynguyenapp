import {
    SET_COURSE, SET_LESSON,
    SET_COURSES, SET_USERINFO,
    CHANGE_ROUTE
}                           from '../ActionTypes';

let initialState = {
    route:"HOMEPAGE",
    courses: {
        lop10: [],
        lop11: [],
        lop12: []
    },
    course: {},
    lesson: {},
    userinfo: {
        courses:[]
    }
};
  
const app = (state = initialState, action) => {
    switch (action.type) {
        case SET_USERINFO:
            return {
                ...state,
                userinfo: action.userinfo,
                route: "HOMEPAGE"
            }
        case SET_LESSON:
            return{
                ...state,
                lesson: action.lesson,
                route: "LESSON"
            }
        case CHANGE_ROUTE:
            return {
                ...state,
                route: action.route
            } 
        case SET_COURSE:
            return{
                ...state,
                course: action.course,
                route: "COURSE"
            }
        case SET_COURSES:
            switch(action.level){
                case "lop12":
                    return {
                        ...state, 
                        courses: {
                            ...state.courses,
                            lop12: action.courses
                        }
                    }
                case "lop11":
                    return {
                        ...state, 
                        courses: {
                            ...state.courses,
                            lop11: action.courses
                        }
                    }
                case "lop10":
                    return {
                        ...state, 
                        courses: {
                            ...state.courses,
                            lop10: action.courses
                        }
                    }
            }
        default:
            return state
    }
}
  
export default app;