import React,{Component}                    from 'react';
import { 
    Text, View, TouchableHighlight,
    ScrollView, Alert
}                                           from 'react-native'; 
import YouTube                              from 'react-native-youtube';
import { connect }                          from 'react-redux';
import { getLesson, buyCourse }             from './../actions/app';
import styles                               from './../styles';

class CourseDetail extends Component {
    constructor(props){
        super(props);
        this.goLesson       = this.goLesson.bind(this);
        this.onBuyCourse    = this.onBuyCourse.bind(this);
    }

    goLesson(idLesson){
        this.props.dispatch(getLesson(idLesson));
    }

    onBuyCourse(courseId){
        Alert.alert(
            'Cảnh báo !',
            'Bạn có chắc chắn mua khoá học này ?',
            [
                {
                    text: 'Huỷ bỏ'
                },
                {
                    text: 'Đồng ý', 
                    onPress: ()=>this.props.dispatch(buyCourse(courseId))
                }
            ],
            {cancelable: true},
        );
    }

    render(){
        let c = this.props.course;
        if(c.hasOwnProperty('id')){
            let checkBought = this.props.userinfo.courses.findIndex(course=>course.id == c.id);
            return(
                <ScrollView style={styles.homepage}>
                    <Text style={styles.homepageCourseTitle}>{c.title}</Text>
                    <YouTube
                        videoId={c.video}   // The YouTube video ID
                        play={true}             // control playback of video with true/false
                        fullscreen={false}       // control whether the video should play in fullscreen or inline
                        loop={true}             // control whether the video should loop when ended
                        apiKey={"AIzaSyD-vmkDwARyGhkeLcxXh04CG-wmaBfXwRk"}
                        onReady={e => this.setState({ isReady: true })}
                        onChangeState={e => this.setState({ status: e.state })}
                        onChangeQuality={e => this.setState({ quality: e.quality })}
                        onError={e => this.setState({ error: e.error })}
                        style={{ width: '100%', height: 230 }}
                    />
                    <Text style={styles.textBold}>{"Giáo viên: "+c.author}</Text>
                    {
                        checkBought == -1 ?
                        <TouchableHighlight onPress={this.onBuyCourse.bind(this,c.id)}>
                            <Text style={styles.btnPrimary}>Mua ngay</Text>
                        </TouchableHighlight>
                        :
                        <TouchableHighlight>
                            <Text style={styles.btnSuccess}>Đã mua</Text>
                        </TouchableHighlight>
                    }
                    <Text style={styles.textBold}>{"Mô tả khoá học:"}</Text>
                    <Text>{c.description}</Text>
                    <Text style={styles.textBold}>{"Danh sách bài học"}</Text>
                    {
                        c.topics.map(t=>{
                            return(
                                <View key={"t"+t.id}>
                                    <Text style={styles.textBold}>{t.title}</Text>
                                    {
                                        t.lessons.map(l=>{
                                            return(
                                                <TouchableHighlight key={"l"+l.id} onPress={this.goLesson.bind(this,l.id)}>
                                                    <Text style={styles.textPadding2}>{"* "+l.title}</Text>
                                                </TouchableHighlight>
                                            )
                                        })
                                    }
                                </View>
                            )
                        })
                    }
                </ScrollView>
            )
        }else{
            return(
                <ScrollView style={styles.homepage}>
                    <Text>Loading...</Text>
                </ScrollView>
            )
        }
    }
}

function _mapStateToPropsTop(state) {
	return {
        course   : state.app.course,
        userinfo : state.app.userinfo
	};
}
export default connect(_mapStateToPropsTop)(CourseDetail);