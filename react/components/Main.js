import React, {Component}   from 'react';
import Drawer               from 'react-native-drawer';
import { connect }          from 'react-redux';
import { 
    Navigation, CourseDetail,
    Header, Homepage, Login,
    LessonDetail, Methodology,
    Dev, Register
}                                       from './index';
import { initApp }                      from './../actions/app';
import Orientation                      from 'react-native-orientation';

class Main extends Component {
    constructor(props){
        super(props);
    }

    componentDidMount(){
        this.props.dispatch(initApp());
        Orientation.lockToPortrait();
    }

    closeDrawer = () => {
        this._drawer.close()
    };

    openDrawer = () => {
        this._drawer.open()
    };

    render() {
        return (
            <Drawer
                ref={(ref) => this._drawer = ref}
                openDrawerOffset={0.5}
                tapToClose={true}
                content={<Navigation closeDrawer={this.closeDrawer} />}
                >
                <Header openDrawer={this.openDrawer}/>
                { this.props.route == "HOMEPAGE" && <Homepage /> }
                { this.props.route == "COURSE" && <CourseDetail /> }
                { this.props.route == "LESSON" && <LessonDetail /> }
                { this.props.route == "METHODOLOGY" && <Methodology /> }
                { this.props.route == "LOGIN" && <Login /> }
                { this.props.route == "REGISTER" && <Register /> }
                { this.props.route == "DEV" && <Dev /> }
            </Drawer>
        );
    }
}

function _mapStateToPropsTop(state) {
	return {
        route   : state.app.route,
        userinfo: state.app.userinfo
	};
}
export default connect(_mapStateToPropsTop)(Main);
