import React,{Component}            from 'react';
import { 
    Text, ScrollView, View,
    TextInput, TouchableHighlight
}                                   from 'react-native'; 
import IconFontAwesome              from 'react-native-vector-icons/FontAwesome';
import { connect }                  from 'react-redux';
import styles                       from './../styles';
import { submitLogin, changeRoute } from './../actions/app';

class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            username: "",
            password: ""
        }
        this._submitLogin = this._submitLogin.bind(this);
    }

    componentDidMount(){
        if(this.props.userinfo.hasOwnProperty('id')){
            this.props.dispatch(changeRoute("HOMEPAGE"));
        }
    }

    _checkPhone = p => {
        var phoneRe = /(0[1-9]{1}[0-9]{8})$/;
        return phoneRe.test(p);
    }

    _checkPhoneOld = p => {
        var phoneRe = /(01[1-9]{1}[0-9]{8})$/;
        return phoneRe.test(p);
    }

    _checkEmail = e => {
        var emailRe = /([0-9a-zA-Z]+@gmail.com)$/;
        return emailRe.test(e);
    }

    _submitLogin(){
        if(!this._checkEmail(this.state.username) && !this._checkPhoneOld(this.state.username) && !this._checkPhone(this.state.username)){
            alert("Tên đăng nhập phải là số điện thoại hoặc gmail");
            return;
        }
        this.props.dispatch(submitLogin(this.state.username, this.state.password));
    }

    render(){
        return(
            <ScrollView style={styles.loginWrap}>
                <View style={styles.login}>
                    <Text style={styles.loginText}>Thông tin đăng nhập</Text>
                    <View style={styles.loginInput} >
                        <IconFontAwesome 
                            style={styles.loginIcon} 
                            name="user" size={20} />
                        <TextInput
                            accessibilityLabel={'Số điện thoại'}
                            autoCapitalize='none'
                            autoComplete={false}
                            autoCorrect={false}
                            autoFocus={false}
                            keyboardType={'phone-pad'}
                            onChangeText={(username)=>{this.setState({username})}}
                            placeholder={"Số điện thoại"}
                            placeholderTextColor='gray'
                            style={styles.loginTextInput}
                            underlineColorAndroid='transparent'
                            value={this.state.username} />
                    </View>
                    <View style={styles.loginInput} >
                        <IconFontAwesome 
                            style={styles.loginIcon} 
                            name="lock" size={20} />
                        <TextInput
                            accessibilityLabel={'Mật khẩu'}
                            autoCapitalize='none'
                            autoComplete={false}
                            autoCorrect={false}
                            autoFocus={false}
                            secureTextEntry
                            onChangeText={(password)=>{this.setState({password})}}
                            placeholder={"Mật khẩu"}
                            placeholderTextColor='gray'
                            style={styles.loginTextInput}
                            underlineColorAndroid='transparent'
                            value={this.state.password} />
                    </View>
                    <View style={styles.loginInput} >
                        <TouchableHighlight 
                            onPress={this._submitLogin}
                            style={styles.loginButton}
                            accessibilityLabel={"Đăng nhập"}>
                            <Text>Đăng nhập</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

function _mapStateToPropsTop(state) {
	return {
        userinfo   : state.app.userinfo
	};
}
export default connect(_mapStateToPropsTop)(Login);