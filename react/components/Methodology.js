import React,{Component}        from 'react';
import { ScrollView, Text }     from 'react-native'; 
import { connect }              from 'react-redux';
import styles                   from './../styles';

class Methodology extends Component {
    constructor(props){
        super(props);
    }
    render(){
        return(
            <ScrollView style={styles.homepage}>
                <Text style={styles.textBold}>CÁCH ĐĂNG KÝ HỌC TOÁN THẦY NGUYỆN</Text>
                <Text>Bước 1: Xin phép phụ huynh</Text>
                <Text>Bước 2: Đăng ký tài khoản (nhớ đăng kí bằng số điện thoại của em nhé)</Text>
                <Text>Bước 3: Chuyển khoản cho thầy theo thông tin sau: </Text>
                <Text style={styles.textPadding2}> - Chủ khoản: ĐINH TIẾN NGUYỆN</Text>
                <Text style={styles.textPadding2}> - Số tài khoản: 2681 0000 2310 87</Text>
                <Text style={styles.textPadding2}> - Chủ khoản: ĐINH TIẾN NGUYỆN</Text>
                <Text style={styles.textPadding2}> - Ngân hàng Đầu tư và phát triển (BIDV), chi nhánh BIDV Đống Đa</Text>
                <Text style={styles.textPadding2}> - Số tiền chuyển: bằng học phí khóa học em đăng kí</Text>
                <Text style={styles.textPadding2}> - Nội dung chuyển khoản: Số điện thoại em đăng kí</Text>
                <Text>Bước 4: Em nhắn tin cho thầy (0918 229 783) là đã chuyển khoản và đăng kí khóa học nào? Thầy sẽ cộng tiền vào tài khoản của em trên web. Từ đó, em có thể mua khóa học</Text>
            </ScrollView>
        )
    }
}

function _mapStateToPropsTop(state) {
	return {
        route   : state.app.route
	};
}
export default connect(_mapStateToPropsTop)(Methodology);