import React,{Component}                    from 'react';
import { 
    Text, View, ScrollView, 
    TouchableHighlight, Linking
}                                           from 'react-native';
import { connect }                          from 'react-redux';
import VideoPlayer                          from 'react-native-video-controls';
import { getLesson }                        from './../actions/app';
import styles                               from './../styles';
import Orientation                      from 'react-native-orientation';

class LessonDetail extends Component {
    constructor(props){
        super(props);
        this.goLesson = this.goLesson.bind(this);
    }

    componentWillMount(){
        Orientation.unlockAllOrientations();
    }

    goLesson(idLesson){
        this.props.dispatch(getLesson(idLesson));
    }

    render(){
        let c = this.props.course;
        let l = this.props.lesson;
        if(l.hasOwnProperty('id')){
            return(
                <ScrollView style={styles.homepage}>
                    <Text style={styles.homepageCourseTitle}>{l.title}</Text>
                    <VideoPlayer
                        key = {l.id}
                        source={{ uri: l.video }}
                        style={{width: "100%", height: 300}}
                        paused={false}
                    />
                    <Text style={styles.textBold}>{"Giáo viên: "+l.author}</Text>
                    <View style={styles.lessonTextWrap}>
                        <Text style={styles.lessonText}     
                            onPress={() => Linking.openURL('https://drive.google.com/file/d/'+l.baitap+'/view')}>Bài tập về nhà</Text>
                        <Text style={styles.lessonText}
                            onPress={() => Linking.openURL('https://drive.google.com/file/d/'+l.baitap+'/view')}>Đáp án</Text>
                    </View>
                    <Text style={styles.textBold}>{"Danh sách bài học"}</Text>
                        {
                            c.topics.map(t=>{
                                return(
                                    <View key={"t"+t.id}>
                                        <Text style={styles.textBold}>{t.title}</Text>
                                        {
                                            t.lessons.map(lesson=>{
                                                return(
                                                    <TouchableHighlight key={"l"+lesson.id} onPress={this.goLesson.bind(this,lesson.id)}>
                                                        <Text style={l.id == lesson.id? [styles.textPadding2,styles.textActive]: styles.textPadding2}>{"* "+lesson.title}</Text>
                                                    </TouchableHighlight>
                                                )
                                            })
                                        }
                                    </View>
                                )
                            })
                        }
                </ScrollView>
            )
        }else{
            return(
                <ScrollView style={styles.homepage}>
                    <Text>Loading...</Text>
                </ScrollView>
            )
        }
    }
}

function _mapStateToPropsTop(state) {
	return {
        course   : state.app.course,
        lesson   : state.app.lesson
	};
}
export default connect(_mapStateToPropsTop)(LessonDetail);