import React,{Component}            from 'react';
import { 
    Text, ScrollView, View,
    TextInput, TouchableHighlight
}                                   from 'react-native'; 
import IconFontAwesome              from 'react-native-vector-icons/FontAwesome';
import { connect }                  from 'react-redux';
import styles                           from './../styles';
import { submitRegister, changeRoute }  from './../actions/app';

class Register extends Component {
    constructor(props){
        super(props);
        this.state = {
            phone: "",
            password: "",
            repasssword:"",
            fullname:"",
            email:""
        }
        this._submitRegister = this._submitRegister.bind(this);
    }

    componentDidMount(){
        if(this.props.userinfo.hasOwnProperty('id')){
            this.props.dispatch(changeRoute("HOMEPAGE"));
        }
    }

    _checkPhone = p => {
        var phoneRe = /(0[1-9]{1}[0-9]{8})$/;
        return phoneRe.test(p);
    }

    _checkEmail = e => {
        var emailRe = /([0-9a-zA-Z]+@gmail.com)$/;
        return emailRe.test(e);
    }

    _submitRegister(){
        if(!this._checkPhone(this.state.phone)){
            alert("Số điện thoại chưa đúng định dạng!");
            return;
        }
        if(this.state.fullname.length < 5 || this.state.fullname.length > 25){
            alert("Họ tên phải có độ dài từ 5->25 kí tự!");
            return;
        }
        if(!this._checkEmail(this.state.email)){
            alert("Phải dùng gmail!");
            return;
        }
        if(this.state.password != this.state.repassword){
            alert("Mật khẩu nhập lại chưa đúng !");
            return;
        }
        let data = {
            phone: this.state.phone, 
            password: this.state.password,
            email: this.state.email, 
            fullname: this.state.fullname
        }
        this.props.dispatch(submitRegister(data));
    }

    render(){
        return(
            <ScrollView style={styles.loginWrap}>
                <View style={styles.login}>
                    <Text style={styles.loginText}>Thông tin đăng ký</Text>
                    <View style={styles.loginInput} >
                        <IconFontAwesome 
                            style={styles.loginIcon} 
                            name="phone" size={20} />
                        <TextInput
                            accessibilityLabel={'Số điện thoại'}
                            autoCapitalize='none'
                            keyboardType={'phone-pad'}
                            autoComplete={false}
                            autoCorrect={false}
                            autoFocus={false}
                            onChangeText={(phone)=>{this.setState({phone})}}
                            placeholder={"Số điện thoại"}
                            placeholderTextColor='gray'
                            style={styles.loginTextInput}
                            underlineColorAndroid='transparent'
                            value={this.state.phone} />
                    </View>
                    <View style={styles.loginInput} >
                        <IconFontAwesome 
                            style={styles.loginIcon} 
                            name="user" size={20} />
                        <TextInput
                            accessibilityLabel={'Họ tên'}
                            autoCapitalize='none'
                            autoComplete={false}
                            autoCorrect={false}
                            autoFocus={false}
                            onChangeText={(fullname)=>{this.setState({fullname})}}
                            placeholder={"Họ tên"}
                            placeholderTextColor='gray'
                            style={styles.loginTextInput}
                            underlineColorAndroid='transparent'
                            value={this.state.fullname} />
                    </View>
                    <View style={styles.loginInput} >
                        <IconFontAwesome 
                            style={styles.loginIcon} 
                            name="envelope" size={20} />
                        <TextInput
                            accessibilityLabel={'Email'}
                            autoCapitalize='none'
                            autoComplete={false}
                            autoCorrect={false}
                            autoFocus={false}
                            onChangeText={(email)=>{this.setState({email})}}
                            placeholder={"Email"}
                            placeholderTextColor='gray'
                            style={styles.loginTextInput}
                            underlineColorAndroid='transparent'
                            value={this.state.email} />
                    </View>
                    <View style={styles.loginInput} >
                        <IconFontAwesome 
                            style={styles.loginIcon} 
                            name="lock" size={20} />
                        <TextInput
                            accessibilityLabel={'Mật khẩu'}
                            autoCapitalize='none'
                            autoComplete={false}
                            autoCorrect={false}
                            autoFocus={false}
                            secureTextEntry
                            onChangeText={(password)=>{this.setState({password})}}
                            placeholder={"Mật khẩu"}
                            placeholderTextColor='gray'
                            style={styles.loginTextInput}
                            underlineColorAndroid='transparent'
                            value={this.state.password} />
                    </View>
                    <View style={styles.loginInput} >
                        <IconFontAwesome 
                            style={styles.loginIcon} 
                            name="lock" size={20} />
                        <TextInput
                            accessibilityLabel={'Nhập lại mật khẩu'}
                            autoCapitalize='none'
                            autoComplete={false}
                            autoCorrect={false}
                            autoFocus={false}
                            secureTextEntry
                            onChangeText={(repassword)=>{this.setState({repassword})}}
                            placeholder={"Nhập lại mật khẩu"}
                            placeholderTextColor='gray'
                            style={styles.loginTextInput}
                            underlineColorAndroid='transparent'
                            value={this.state.repassword} />
                    </View>
                    <View style={styles.loginInput} >
                        <TouchableHighlight 
                            onPress={this._submitRegister}
                            style={styles.loginButton}
                            accessibilityLabel={"Đăng ký"}>
                            <Text>Đăng ký</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </ScrollView>
        )
    }
}

function _mapStateToPropsTop(state) {
	return {
        userinfo   : state.app.userinfo
	};
}
export default connect(_mapStateToPropsTop)(Register);