import React,{Component}        from 'react';
import { connect }              from 'react-redux';
import { 
    Text, View, 
    Image, TouchableHighlight 
}                                   from 'react-native'; 
import Icon                         from 'react-native-vector-icons/MaterialIcons';
import IconFontAwesome              from 'react-native-vector-icons/FontAwesome';
import styles                       from './../styles';
import { changeRoute, submitLogout }from './../actions/app';

class Navigation extends Component {
    constructor(props){
        super(props);
        this.changeRoute = this.changeRoute.bind(this);
        this.logout      = this.logout.bind(this);
    }

    changeRoute(route){
        this.props.dispatch(changeRoute(route));
        this.props.closeDrawer();
    }

    logout(){
        this.props.dispatch(submitLogout());
        this.props.dispatch(changeRoute("HOMEPAGE"));
    }

    render(){
        return(
            <View style={styles.navi}>
                <View style={styles.naviHeader}>
                    <View style={styles.naviHeaderInfo}>
                        <Image
                            style={styles.naviHeaderImg}
                            source={{uri: 'https://www.thestylease.com/images/user.png'}}
                        />
                        {
                            this.props.userinfo.hasOwnProperty('id') ?
                            <View>
                                <Text>{"Xin chào: "+this.props.userinfo.fullname}</Text>
                                <Text>{"Số dư: "+this.props.userinfo.balance+" 000đ"}</Text>
                                <TouchableHighlight onPress={this.logout}>
                                    <Text>Đăng xuất</Text>
                                </TouchableHighlight>
                            </View>
                            :
                            <View>
                                <TouchableHighlight onPress={this.changeRoute.bind(this,"LOGIN")}>
                                    <Text>Đăng nhập ngay</Text>
                                </TouchableHighlight>
                                <TouchableHighlight onPress={this.changeRoute.bind(this,"REGISTER")}>
                                    <Text>Đăng ký</Text>
                                </TouchableHighlight>
                            </View>
                        }
                    </View>
                </View>
                <View>
                    <TouchableHighlight onPress={this.changeRoute.bind(this,"HOMEPAGE")}>
                        <View style={styles.naviItem}>
                            <Icon style={styles.naviItemIcon} name="home" size={20} />
                            <Text style={styles.naviItemText}>Trang chủ</Text>
                        </View>
                    </TouchableHighlight>
                    <TouchableHighlight onPress={this.changeRoute.bind(this,"METHODOLOGY")}>
                        <View style={styles.naviItem}>
                            <IconFontAwesome style={styles.naviItemIcon} name="money" size={20} />
                            <Text style={styles.naviItemText}>Nạp tiền</Text>
                        </View>
                    </TouchableHighlight>
                    <View style={styles.naviItem}>
                        <Icon style={styles.naviItemIcon} name="label" size={20} />
                        <Text style={styles.naviItemText}>Khoá học</Text>
                    </View>
                    <View style={styles.naviItem}>
                        <Icon style={styles.naviItemIcon} name="book" size={20} />
                        <Text style={styles.naviItemText}>Tài liệu</Text>
                    </View>
                    <View style={styles.naviItem}>
                        <Icon style={styles.naviItemIcon} name="check" size={20} />
                        <Text style={styles.naviItemText}>Đề thi</Text>
                    </View>
                    <View style={styles.naviItem}>
                        <Icon style={styles.naviItemIcon} name="grade" size={20} />
                        <Text style={styles.naviItemText}>Tin tức</Text>
                    </View>
                </View>
            </View>
        )
    }
}

function _mapStateToPropsTop(state) {
	return {
        userinfo   : state.app.userinfo
	};
}
export default connect(_mapStateToPropsTop)(Navigation);