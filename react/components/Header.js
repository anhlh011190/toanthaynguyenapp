import React,{Component}        from 'react';
import { 
    Text, View, StatusBar 
}                               from 'react-native'; 
import { connect }              from 'react-redux';
import Icon                     from 'react-native-vector-icons/MaterialIcons';
import IconFontAwesome          from 'react-native-vector-icons/FontAwesome';
import styles                   from './../styles';
import { changeRoute }          from './../actions/app';

class Header extends Component {
    constructor(props){
        super(props);
    }
    render(){
        return(
            <View style={styles.header}>
            {
                this.props.route == "LESSON" && 
                <IconFontAwesome 
                    style={styles.headerIconBack} 
                    onPress={()=>{this.props.dispatch(changeRoute("COURSE"))}} 
                    name="chevron-left" size={20} />
            }
            {
                this.props.route != "HOMEPAGE" && this.props.route != "LESSON" && 
                <IconFontAwesome 
                    style={styles.headerIconBack} 
                    onPress={()=>{this.props.dispatch(changeRoute("HOMEPAGE"))}} 
                    name="chevron-left" size={20} />
            }
                <Text style={styles.headerText}>TOÁN THẦY NGUYỆN</Text>
                <Icon style={styles.headerIcon} onPress={this.props.openDrawer} name="menu" size={30} />
                <StatusBar hidden={true} />
            </View>
        )
    }
}

function _mapStateToPropsTop(state) {
	return {
        route     : state.app.route
	};
}
export default connect(_mapStateToPropsTop)(Header);