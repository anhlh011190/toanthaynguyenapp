import React,{Component}                    from 'react';
import { 
    Text, View, Image,
    ScrollView, TouchableHighlight 
}                                           from 'react-native'; 
import { connect }                          from 'react-redux';
import styles                               from './../styles';
import { getCourse }                    from './../actions/app';

class Homepage extends Component {
    constructor(props){
        super(props);
        this.goCourse = this.goCourse.bind(this);
    }

    goCourse(idCourse){
        this.props.dispatch(getCourse(idCourse));
    }

    render(){
        return(
            <ScrollView
                style={styles.homepage}>
                <Text style={styles.homepageCourseTitle}>* KHÓA HỌC DÀNH CHO HỌC SINH LỚP 12 VÀ ÔN THI ĐẠI HỌC</Text>
                {
                    this.props.courses.lop12.map(c=>{
                        return(
                            <View key={c.id} style={styles.homepageCourseList}>
                                <TouchableHighlight onPress={this.goCourse.bind(this,c.id)} style={styles.homepageCourseListImgs}>
                                    <Image
                                        style={styles.homepageCourseListImg}
                                        source={{uri: 'https://drive.google.com/uc?id='+c.thumb+'&export=download'}}
                                    />
                                </TouchableHighlight>
                                <TouchableHighlight onPress={this.goCourse.bind(this,c.id)} style={styles.homepageCourseListContent}>
                                    <View>
                                        <Text>{c.title}</Text>
                                        <Text style={styles.homepageCourseListContentText}>{"Giá: "+c.price+" 000đ"}</Text>
                                        <Text style={styles.homepageCourseListContentText}>{"Ngày khai giảng: "+c.start}</Text>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        )
                    })
                }
                <Text style={styles.homepageCourseTitle}>* KHÓA HỌC DÀNH CHO HỌC SINH LỚP 11</Text>
                {
                    this.props.courses.lop11.map(c=>{
                        return(
                            <View key={c.id} style={styles.homepageCourseList}>
                                <TouchableHighlight onPress={this.goCourse.bind(this,c.id)} style={styles.homepageCourseListImgs}>
                                    <Image
                                        style={styles.homepageCourseListImg}
                                        source={{uri: 'https://drive.google.com/uc?id='+c.thumb+'&export=download'}}
                                    />
                                </TouchableHighlight>
                                <TouchableHighlight onPress={this.goCourse.bind(this,c.id)} style={styles.homepageCourseListContent}>
                                    <View>
                                        <Text>{c.title}</Text>
                                        <Text style={styles.homepageCourseListContentText}>{"Giá: "+c.price+" 000đ"}</Text>
                                        <Text style={styles.homepageCourseListContentText}>{"Ngày khai giảng: "+c.start}</Text>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        )
                    })
                }
                <Text style={styles.homepageCourseTitle}>* KHÓA HỌC DÀNH CHO HỌC SINH LỚP 10</Text>
                {
                    this.props.courses.lop10.map(c=>{
                        return(
                            <View key={c.id} style={styles.homepageCourseList}>
                                <TouchableHighlight onPress={this.goCourse.bind(this,c.id)} style={styles.homepageCourseListImgs}>
                                    <Image
                                        style={styles.homepageCourseListImg}
                                        source={{uri: 'https://drive.google.com/uc?id='+c.thumb+'&export=download'}}
                                    />
                                </TouchableHighlight>
                                <TouchableHighlight onPress={this.goCourse.bind(this,c.id)} style={styles.homepageCourseListContent}>
                                    <View>
                                        <Text>{c.title}</Text>
                                        <Text style={styles.homepageCourseListContentText}>{"Giá: "+c.price+" 000đ"}</Text>
                                        <Text style={styles.homepageCourseListContentText}>{"Ngày khai giảng: "+c.start}</Text>
                                    </View>
                                </TouchableHighlight>
                            </View>
                        )
                    })
                }
            </ScrollView>
        )
    }
}

function _mapStateToPropsTop(state) {
	return {
        courses   : state.app.courses
	};
}
export default connect(_mapStateToPropsTop)(Homepage);